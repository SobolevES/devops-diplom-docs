_____
## Исходное задание можно посмотреть [здесь](https://gitlab.com/SobolevES/devops-diplom-yandexcloud)  

------

### Ход выполнения:  

# Дипломный практикум в Яндекс.Облако
  * [Цели:](#цели)
  * [Этапы выполнения:](#этапы-выполнения)
     * [Создание облачной инфраструктуры](#создание-облачной-инфраструктуры)
     * [Создание Kubernetes кластера](#создание-kubernetes-кластера)
     * [Создание тестового приложения](#создание-тестового-приложения)
     * [Подготовка cистемы мониторинга и деплой приложения](#подготовка-cистемы-мониторинга-и-деплой-приложения)
     * [Установка и настройка CI/CD](#установка-и-настройка-cicd)
  * [Что необходимо для сдачи задания?](#что-необходимо-для-сдачи-задания)
  * [Как правильно задавать вопросы дипломному руководителю?](#как-правильно-задавать-вопросы-дипломному-руководителю)

---
## Цели:

1. Подготовить облачную инфраструктуру на базе облачного провайдера Яндекс.Облако.
2. Запустить и сконфигурировать Kubernetes кластер.
3. Установить и настроить систему мониторинга.
4. Настроить и автоматизировать сборку тестового приложения с использованием Docker-контейнеров.
5. Настроить CI для автоматической сборки и тестирования.
6. Настроить CD для автоматического развёртывания приложения.

---
## Этапы выполнения:


### Создание облачной инфраструктуры

Для начала необходимо подготовить облачную инфраструктуру в ЯО при помощи [Terraform](https://www.terraform.io/).

Особенности выполнения:

- Бюджет купона ограничен, что следует иметь в виду при проектировании инфраструктуры и использовании ресурсов;
- Следует использовать последнюю стабильную версию [Terraform](https://www.terraform.io/).

Предварительная подготовка к установке и запуску Kubernetes кластера.

1. Создайте сервисный аккаунт, который будет в дальнейшем использоваться Terraform для работы с инфраструктурой с необходимыми и достаточными правами. Не стоит использовать права суперпользователя
2. Подготовьте [backend](https://www.terraform.io/docs/language/settings/backends/index.html) для Terraform:  
   а. Рекомендуемый вариант: [Terraform Cloud](https://app.terraform.io/)  
   б. Альтернативный вариант: S3 bucket в созданном ЯО аккаунте
3. Настройте [workspaces](https://www.terraform.io/docs/language/state/workspaces.html)  
   а. Рекомендуемый вариант: создайте два workspace: *stage* и *prod*. В случае выбора этого варианта все последующие шаги должны учитывать факт существования нескольких workspace.  
   б. Альтернативный вариант: используйте один workspace, назвав его *stage*. Пожалуйста, не используйте workspace, создаваемый Terraform-ом по-умолчанию (*default*).
4. Создайте VPC с подсетями в разных зонах доступности.
5. Убедитесь, что теперь вы можете выполнить команды `terraform destroy` и `terraform apply` без дополнительных ручных действий.
6. В случае использования [Terraform Cloud](https://app.terraform.io/) в качестве [backend](https://www.terraform.io/docs/language/settings/backends/index.html) убедитесь, что применение изменений успешно проходит, используя web-интерфейс Terraform cloud.

Ожидаемые результаты:

1. Terraform сконфигурирован и создание инфраструктуры посредством Terraform возможно без дополнительных ручных действий.
2. Полученная конфигурация инфраструктуры является предварительной, поэтому в ходе дальнейшего выполнения задания возможны изменения.

___
### Решение этапа "Создание облачной инфраструктуры":  

Первым шагом нам потребуется [установить/обновить](https://learn.hashicorp.com/tutorials/terraform/install-cli) Terraform.  
Вторым шагом нам потребуется [установить](https://cloud.yandex.ru/docs/cli/quickstart#install) `yc` для работы с командной строкой.  
  

Далее создаем сервисный акккаун и настраиваем его:  
```
yc iam service-account create terraform
yc iam key create --service-account-name terraform -o terraform.json
yc config set service-account-key terraform.json
yc iam create-token
SERVICE_ACCOUNT_ID=$(yc iam service-account get --name terraform --format json | jq -r .id)
FOLDER_ID=$(yc iam service-account get --name terraform --format json | jq -r .folder_id)
yc resource-manager folder add-access-binding $FOLDER_ID --role editor --subject serviceAccount:$SERVICE_ACCOUNT_ID
```

Готовим [конфиг](https://gitlab.com/SobolevES/diplom-terraform/-/blob/main/backend.tf) для бекенда.  
  
Создаем окружение (создал оба воркспейса, но далее только с одним работал):  
```
terraform workspace new stage && terraform workspace new prod
terraform workspace select stage
```
  

Готовим конфиг для инфраструктуры. [Здесь](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/compute_instance_group) и [здесь](https://cloud.yandex.ru/docs/compute/operations/instance-groups/create-with-balancer) взял исходник для инстанс-групп, немного модифицировал, добавил балансировщик.  
Все итоговые [конфигурационные файлы](https://gitlab.com/SobolevES/diplom-terraform) выложил в отдельный репозиторий.  
  
После выполнения `terraform init && terraform apply` проверяем корректность поднятия наших виртуальных машин в Yandex Cloud:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-yc-instance.png)   

А также в terraform.io:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-terraform-io.png)   


---
### Создание Kubernetes кластера

На этом этапе необходимо создать [Kubernetes](https://kubernetes.io/ru/docs/concepts/overview/what-is-kubernetes/) кластер на базе предварительно созданной инфраструктуры.   Требуется обеспечить доступ к ресурсам из Интернета.

Это можно сделать двумя способами:

1. Рекомендуемый вариант: самостоятельная установка Kubernetes кластера.  
   а. При помощи Terraform подготовить как минимум 3 виртуальных машины Compute Cloud для создания Kubernetes-кластера. Тип виртуальной машины следует выбрать самостоятельно с учётом требовании к производительности и стоимости. Если в дальнейшем поймете, что необходимо сменить тип инстанса, используйте Terraform для внесения изменений.  
   б. Подготовить [ansible](https://www.ansible.com/) конфигурации, можно воспользоваться, например [Kubespray](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/)  
   в. Задеплоить Kubernetes на подготовленные ранее инстансы, в случае нехватки каких-либо ресурсов вы всегда можете создать их при помощи Terraform.
2. Альтернативный вариант: воспользуйтесь сервисом [Yandex Managed Service for Kubernetes](https://cloud.yandex.ru/services/managed-kubernetes)  
  а. С помощью terraform resource для [kubernetes](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_cluster) создать региональный мастер kubernetes с размещением нод в разных 3 подсетях      
  б. С помощью terraform resource для [kubernetes node group](https://registry.terraform.io/providers/yandex-cloud/yandex/latest/docs/resources/kubernetes_node_group)
  
Ожидаемый результат:

1. Работоспособный Kubernetes кластер.
2. В файле `~/.kube/config` находятся данные для доступа к кластеру.
3. Команда `kubectl get pods --all-namespaces` отрабатывает без ошибок.
---
### Решение этапа "Создание Kubernetes кластера":  

На основе kubespray готовим конфигурацию для ansible.  
Склонировал себе [репозиторий](https://kubernetes.io/docs/setup/production-environment/tools/kubespray/), далее выполнил все действия, описанные в документации.  
Получился [инвентори-файл](https://gitlab.com/SobolevES/devops-diplom-kubespray/-/blob/main/hosts.yaml) с моими хостами.  
  
Далее запуск плейбука:  
`ansible-playbook -i inventory/mycluster/hosts.yaml cluster.yml -e maximal_ansible_version=2.14.0 -e ansible_user=ubuntu -b --become-user=root -e ansible_ssh_private_key_file=~/.ssh/id_rsa`
  
Результат выполнения плейбука:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-ansible-OK.png)   
  
Подключаемся к мастер-ноде и проверяем корректность инициилизации кластера:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-get-nodes.png)   
  
Проверяем `cat ~/.kube/config` данные для доступа к кластеру:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-kube-config2.png)   
  
Проверяем статус подов во всех неймспейсах:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-get-pods-allnamespace.png)   
  

---
### Создание тестового приложения

Для перехода к следующему этапу необходимо подготовить тестовое приложение, эмулирующее основное приложение разрабатываемое вашей компанией.

Способ подготовки:

1. Рекомендуемый вариант:  
   а. Создайте отдельный git репозиторий с простым nginx конфигом, который будет отдавать статические данные.  
   б. Подготовьте Dockerfile для создания образа приложения.  
2. Альтернативный вариант:  
   а. Используйте любой другой код, главное, чтобы был самостоятельно создан Dockerfile.

Ожидаемый результат:

1. Git репозиторий с тестовым приложением и Dockerfile.
2. Регистр с собранным docker image. В качестве регистра может быть DockerHub или [Yandex Container Registry](https://cloud.yandex.ru/services/container-registry), созданный также с помощью terraform.
  
---
### Решение этапа "Создание тестового приложения":  
  
GitLab я использую уже давно для ci/cd, поэтому для меня выбор был очевиден.  
Создал [репозиторий](https://gitlab.com/SobolevES/devops-diplom-app) для тестового приложения на основе nginx.  
Далее настроил его для работы с CI/CD.  Подготовил файлы, описал переменные среды.  
В итоге после коммита в репозиторий начинается собираться пайплнайн.  

Проверяем, что job успешно отработал:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-gitlab-cicd.png)   
  
Проверяем, что в [container Registry](https://gitlab.com/SobolevES/devops-diplom-app/container_registry/) образ запушился успешно:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-container-regystry.png)   
  
Проверяем, что в [dockerHub](https://hub.docker.com/repository/docker/soboleves/devops-diplom-app) успешно запушился наш образ:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-dockerhub.png)   
  
---
### Подготовка cистемы мониторинга и деплой приложения

Уже должны быть готовы конфигурации для автоматического создания облачной инфраструктуры и поднятия Kubernetes кластера.  
Теперь необходимо подготовить конфигурационные файлы для настройки нашего Kubernetes кластера.

Цель:
1. Задеплоить в кластер [prometheus](https://prometheus.io/), [grafana](https://grafana.com/), [alertmanager](https://github.com/prometheus/alertmanager), [экспортер](https://github.com/prometheus/node_exporter) основных метрик Kubernetes.
2. Задеплоить тестовое приложение, например, [nginx](https://www.nginx.com/) сервер отдающий статическую страницу.

Рекомендуемый способ выполнения:
1. Воспользовать пакетом [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus), который уже включает в себя [Kubernetes оператор](https://operatorhub.io/) для [grafana](https://grafana.com/), [prometheus](https://prometheus.io/), [alertmanager](https://github.com/prometheus/alertmanager) и [node_exporter](https://github.com/prometheus/node_exporter). При желании можете собрать все эти приложения отдельно.
2. Для организации конфигурации использовать [qbec](https://qbec.io/), основанный на [jsonnet](https://jsonnet.org/). Обратите внимание на имеющиеся функции для интеграции helm конфигов и [helm charts](https://helm.sh/)
3. Если на первом этапе вы не воспользовались [Terraform Cloud](https://app.terraform.io/), то задеплойте в кластер [atlantis](https://www.runatlantis.io/) для отслеживания изменений инфраструктуры.

Альтернативный вариант:
1. Для организации конфигурации можно использовать [helm charts](https://helm.sh/)

Ожидаемый результат:
1. Git репозиторий с конфигурационными файлами для настройки Kubernetes.
2. Http доступ к web интерфейсу grafana.
3. Дашборды в grafana отображающие состояние Kubernetes кластера.
4. Http доступ к тестовому приложению.
---

### Решение этапа "Подготовка cистемы мониторинга и деплой приложения":  
  
Как и предложено в задании, за основу я взял [пакет](https://github.com/prometheus-operator/kube-prometheus)  
Склонировал себе в репозиторий, внес корректировки в манифесты деплоймента и сервисов + 
добавил манифест деплоймента+сервиса для моего тестового приложения.  
  
Основные шаги для исполнения данной задачи:  
Подключился на мастер ноду  
`ssh ubuntu@<ip>`  
Далее склонировал свой репозиторий с манифестами  
`git clone https://github.com/prometheus-operator/kube-prometheus`

Далее применение манифестов:  
```
cd kube-prometheus
kubectl apply --server-side -f manifests/setup
until kubectl get servicemonitors --all-namespaces ; do date; sleep 1; echo ""; done
kubectl apply -f manifests/

### если по какой то причине манифесты в иной папке отличной от manifests/
kubectl apply -f deployment.yaml
kubectl apply -f service.yaml
```
  
//Проверки, после применения манифестов://  
  
Так можно посмотреть сервисы с портами:  
`kubectl --namespace monitoring get svc`
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-get-svc.png)   
  
Так можно посмотреть поды во всех ns:  
`kubectl get pods --all-namespaces -o wide`
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-get-pods-allnamespaces2.png)   
  
Так можно посмотреть более подробную инфу по нодам с ip:  
`kubectl get nodes --output wide`
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-get-nodes-ip.png)  
  
Проверяем, что UI графаны (admin/admin) и тестового приклада поднялись и доступны из вне:  
[Grafana](http://51.250.37.30:30000/d/efa86fd1d0c121a26444b636a3f509a8/kubernetes-compute-resources-cluster?orgId=1&refresh=10s)  >  OK.  
[My__app](http://51.250.37.30:30001/)  >  OK.  
  
UI grafana:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-grafana.png)   
  
UI app:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-ma-app.png)   


---
### Установка и настройка CI/CD

Осталось настроить ci/cd систему для автоматической сборки docker image и деплоя приложения при изменении кода.

Цель:

1. Автоматическая сборка docker образа при коммите в репозиторий с тестовым приложением.
2. Автоматический деплой нового docker образа.

Можно использовать [teamcity](https://www.jetbrains.com/ru-ru/teamcity/), [jenkins](https://www.jenkins.io/) либо [gitlab ci](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

Ожидаемый результат:

1. Интерфейс ci/cd сервиса доступен по http.
2. При любом коммите в репозиторие с тестовым приложением происходит сборка и отправка в регистр Docker образа.
3. При создании тега (например, v1.0.0) происходит сборка и отправка с соответствующим label в регистр, а также деплой соответствующего Docker образа в кластер Kubernetes.

### Решение этапа "Установка и настройка CI/CD":  
  
Первым делом нам нужно задать переменные в gitlab, для возможности подключения к кластеру, докеру.  
Переменные CI взял с офф источника [здесь](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)   
По переменным, которые необходимы для подключения к куберу добавлены комментарии в [.gitlab-ci.yml](https://gitlab.com/SobolevES/devops-diplom-app/-/blob/main/.gitlab-ci.yml).  
Продублирую здесь:  
  
```
    #KUBE_URL  <url кластера в формате           https://ip:6443>
    #KUBE_CA_DATA <certificate-authority-data>   kubectl config view --minify --raw --output 'jsonpath={..cluster.certificate-authority-data}' | base64 -d | openssl x509 -text -out -
    #KUBE_USER_CERT <client-certificate-data>    kubectl config view --minify --raw --output 'jsonpath={..user.client-certificate-data}' | base64 -d | openssl x509 -text -out -
    #KUBE_USER_KEY <client-key-data>             kubectl config view --minify --raw --output 'jsonpath={..user.client-key-data}' | base64 -d
```
В итоге сформировался такой список переменных:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-variables.png)   
  
За основу файла конфигурации я взал вот [этот](https://cloud.yandex.ru/docs/tutorials/infrastructure-management/gitlab-containers) пример от Яндекса.  

Далее сборка проекта и запуск пайплайна.  
Результат:  
Образ собрался и запушился в [Docker.Hub](https://hub.docker.com/repository/docker/soboleves/devops-diplom-app)  
  
Пайплайн успешно отработал на всех стадиях:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-success-pipline.png)   
  
Подключаемся на мастера, проверяем корректность деплоя манифеста:  
![Screenshot](https://gitlab.com/SobolevES/devops-netology/-/raw/main/pics/diplom-after-deploy.png)   
  
Деплоймент успешно обновился.  
Поведение ожидаемое.  






---
## Что необходимо для сдачи задания?

1. Репозиторий с конфигурационными файлами Terraform и готовность продемонстрировать создание всех ресурсов с нуля.  
https://gitlab.com/SobolevES/diplom-terraform  
2. Пример pull request с комментариями созданными atlantis'ом или снимки экрана из Terraform Cloud.  
https://gitlab.com/SobolevES/devops-netology/-/blob/main/pics/diplom-terraform-io.png  
3. Репозиторий с конфигурацией ansible, если был выбран способ создания Kubernetes кластера при помощи ansible.  
https://gitlab.com/SobolevES/devops-diplom-kubespray  
4. Репозиторий с Dockerfile тестового приложения и ссылка на собранный docker image.  
https://gitlab.com/SobolevES/devops-diplom-app  
https://gitlab.com/SobolevES/devops-diplom-app/-/jobs  
https://hub.docker.com/repository/docker/soboleves/devops-diplom-app  
5. Репозиторий с конфигурацией Kubernetes кластера.  
https://gitlab.com/SobolevES/devops-diplom-kubespray  
https://gitlab.com/SobolevES/devops-diplom-k8s.conf  
6. Ссылка на тестовое приложение и веб интерфейс Grafana с данными доступа.  
http://51.250.37.30:30001/  
http://51.250.37.30:30000/d/efa86fd1d0c121a26444b636a3f509a8/kubernetes-compute-resources-cluster?orgId=1&refresh=10s  
7. Все репозитории рекомендуется хранить на одном ресурсе (github, gitlab)  
https://gitlab.com/SobolevES  
---






















######################################################################



полезные ссылки для использования в работе или дебага:  

OAuth-токен (Срок жизни OAuth-токена 1 год)
 https://cloud.yandex.ru/docs/iam/concepts/authorization/oauth-tokenе
 
Получение IAM-токена для аккаунта на Яндексе (Время жизни IAM-токена — не больше 12 часов)  
https://cloud.yandex.ru/docs/iam/operations/iam-token/create


Долго мучился с докер хабом
ОДним из решениев было передавать значение токена через файл
https://docs.docker.com/engine/reference/commandline/login/

https://docs.gitlab.com/ee/user/packages/container_registry/

===========
===========